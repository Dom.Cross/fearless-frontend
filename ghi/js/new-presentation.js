window.addEventListener('DOMContentLoaded', async () => {
    const createPresentationForm = document.getElementById('create-presentation-form');
    const conferenceSelect = document.getElementById('conference');
  
    // Fetch conference data and populate the dropdown
    try {
      const response = await fetch('http://localhost:8000/api/conferences/');
      const data = await response.json();
  
      if (response.ok) {
        for (const conference of data.conferences) {
          const option = document.createElement('option');
          option.value = conference.href.split('/')[3]; // Extract the conference ID from the href
          option.textContent = conference.name;
          conferenceSelect.appendChild(option);
        }
      } else {
        console.error('Failed to fetch conference data');
      }
    } catch (error) {
      console.error('An error occurred while fetching conference data:', error);
    }
  
    createPresentationForm.addEventListener('submit', async (event) => {
      event.preventDefault();
  
      const presenterName = createPresentationForm.elements.presenter_name.value;
      const presenterEmail = createPresentationForm.elements.presenter_email.value;
      const companyName = createPresentationForm.elements.company_name.value;
      const title = createPresentationForm.elements.title.value;
      const synopsis = createPresentationForm.elements.synopsis.value;
  
      const selectedConferenceId = conferenceSelect.value;
  
      const presentation = {
        presenter_name: presenterName,
        presenter_email: presenterEmail,
        company_name: companyName,
        title,
        synopsis,
      };
  
      const url = `http://localhost:8000/api/conferences/${selectedConferenceId}/presentations/`;
      const fetchConfig = {
        method: 'post',
        body: JSON.stringify(presentation),
        headers: {
          'Content-Type': 'application/json',
        },
      };
  
      try {
        const response = await fetch(url, fetchConfig);
  
        if (response.ok) {
          createPresentationForm.reset();
          const newPresentation = await response.json();
          console.log('Presentation created successfully!', newPresentation);
        } else {
          console.error('Failed to create presentation');
        }
      } catch (error) {
        console.error('An error occurred:', error);
      }
    });
  });