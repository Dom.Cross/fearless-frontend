function createCard(conference) {
  const {
    name,
    description,
    starts,
    ends,
    location: { picture_url: pictureUrl },
  } = conference;

  const startDate = moment(starts).format('MM/DD/YY');
  const endDate = moment(ends).format('MM/DD/YY');

  return `
    <div class="card mb-3 shadow">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer">
        <small class="text-muted">${startDate} - ${endDate}</small>
      </div>
    </div>
  `;
}

function createPlaceholderCard() {
  return `
    <div class="card mb-3 shadow">
      <div class="card-body">
        <h5 class="card-title">Loading...</h5>
        <p class="card-text">Please wait while the conference details are being loaded.</p>
      </div>
      <div class="card-footer">
        <small class="text-muted">--/--/---- - --/--/----</small>
      </div>
    </div>
  `;
}

window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      throw new Error('Response not ok');
    } else {
      const data = await response.json();

      const columns = [
        document.getElementById("card-column-1"),
        document.getElementById("card-column-2"),
        document.getElementById("card-column-3"),
      ];

      let columnIndex = 0;
      for (let conference of data.conferences) {
        const placeholderHtml = createPlaceholderCard();
        const placeholderElement = document.createElement('div');
        placeholderElement.innerHTML = placeholderHtml;
        columns[columnIndex].appendChild(placeholderElement);

        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const conferenceData = await detailResponse.json();
          const html = createCard(conferenceData.conference);
          placeholderElement.innerHTML = html;
        } else {
          placeholderElement.innerHTML = 'Error loading conference';
        }

        columnIndex = (columnIndex + 1) % columns.length;
      }

    }
  } catch (error) {
    console.error('Error:', error);
  }

});