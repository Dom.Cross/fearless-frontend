window.addEventListener('DOMContentLoaded', async () => {
    const locationsUrl = 'http://localhost:8000/api/locations/';
  
    const response = await fetch(locationsUrl);
  
    if (response.ok) {
      const data = await response.json();
      const selectTag = document.getElementById('location');

      for (let location of data.locations) {
        const option = document.createElement('option');
        option.value = location.id; 
        option.innerHTML = location.name;
        selectTag.appendChild(option);
      }
    }
  
    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async (event) => {
      event.preventDefault();

      const name = formTag.elements.name.value;
      const starts = formTag.elements.starts.value;
      const ends = formTag.elements.ends.value;
      const description = formTag.elements.description.value;
      const maxPresentations = parseInt(formTag.elements.max_presentations.value);
      const maxAttendees = parseInt(formTag.elements.max_attendees.value);
      const location = parseInt(formTag.elements.location.value);

      const conference = {
        name,
        starts,
        ends,
        description,
        max_presentations: maxPresentations,
        max_attendees: maxAttendees,
        location,
      };

      const conferenceUrl = 'http://localhost:8000/api/conferences/';
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(conference),
        headers: {
          'Content-Type': 'application/json',
        },
      };

      const response = await fetch(conferenceUrl, fetchConfig);
  
      if (response.ok) {
        formTag.reset();
        console.log('Conference created successfully!');
      } else {
        console.error('Failed to create conference');
      }
    });
  });
