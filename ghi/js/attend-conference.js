async function populateConferences() {
  const selectTag = document.getElementById('conference');

  try {
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }

      const loadingSpinner = document.getElementById('loading-conference-spinner');
      const form = document.getElementById('create-attendee-form');
      loadingSpinner.classList.add('d-none');
      selectTag.classList.remove('d-none');
      form.classList.remove('d-none');
    } else {
      console.error('Error:', response.statusText);
    }
  } catch (error) {
    console.error('Error:', error);
  }
}

async function handleSubmit(event) {
  event.preventDefault();

  const formData = new FormData(event.target);
  const attendeeData = Object.fromEntries(formData.entries());

  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(attendeeData),
  };

  try {
    const response = await fetch('http://localhost:8001/api/attendees/', options);
    if (response.ok) {
      const successMessage = document.getElementById('success-message');
      const form = document.getElementById('create-attendee-form');

      successMessage.classList.remove('d-none');
      form.classList.add('d-none');
    } else {
      console.error('Error:', response.statusText);
    }
  } catch (error) {
    console.error('Error:', error);
  }
}

window.addEventListener('DOMContentLoaded', () => {
  const form = document.getElementById('create-attendee-form');
  form.addEventListener('submit', handleSubmit);

  populateConferences();
});